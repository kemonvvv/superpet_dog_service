package com.superpet.common.kits;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.PropKit;
import com.superpet.common.MainConfig;
import com.superpet.common.model.WxRun;
import com.superpet.common.vo.WxSessionVo;
import com.superpet.weixin.api.wxrun.WxRunService;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static sun.io.Win32ErrorMode.initialize;

public class WeixinKit {

    public static WxSessionVo getWxSession(String jsCode){
        PropKit.use(MainConfig.proFileName);
        Map<String,String> paras = new HashMap<String, String>();
        paras.put("js_code",jsCode);
        paras.put("appid", PropKit.get("appId"));
        paras.put("secret", PropKit.get("appSecret"));
        paras.put("grant_type", "authorization_code");
        Map<String,String> headers = new HashMap<String, String>();
        headers.put("Accept-Language","zh-CN,en-US");
        headers.put("Content-Type", "application/x-www-form-urlencoded");
        String result = com.jfinal.kit.HttpKit.get("https://api.weixin.qq.com/sns/jscode2session",paras,headers);
        System.out.println("result----"+result);
        WxSessionVo sret = JsonKit.parse(result,WxSessionVo.class);
        return sret;
    }

    public static WxRun getWeRunData(WxSessionVo wxSessionVo){
        WxRun wxRun = null;
        try {
            byte[] resultByte = AESKit.instance.decrypt(Base64.decodeBase64(wxSessionVo.getEncryptedData())
                    , Base64.decodeBase64(wxSessionVo.getSession_key()), Base64.decodeBase64(wxSessionVo.getIvData()));
            if(null != resultByte && resultByte.length > 0){
                String runInfo = new String(resultByte, "UTF-8");
                JSONObject json = JSONObject.parseObject(runInfo);
                List<WxRun> wxRunList = JSONObject.parseArray(JSONObject.toJSONString(json.get("stepInfoList")),WxRun.class);
                System.out.println(JsonKit.toJson(wxRunList));
                wxRunList.sort(new Comparator<WxRun>() {
                    @Override
                    public int compare(WxRun o1, WxRun o2) {
                        if(o1.getTimestamp()>o2.getTimestamp()){
                            return 1;
                        }else{
                            return 0;
                        }
                    }
                });
//                wxRunList.sort((o1, o2) ->{
//                    if(o1.getTimestamp()>o2.getTimestamp()){
//                        return 1;
//                    }else{
//                        return 0;
//                    }
//                });
                wxRun = wxRunList.get(wxRunList.size()-2);//获取昨天的
                if(!DateKit.isYesterday(wxRun.getTimestamp() * 1000L)){
                    wxRun = wxRunList.get(wxRunList.size()-1);
                }
                wxRun.setTimestamp(wxRun.getTimestamp() * 1000);
                wxRun.setOpenid(wxSessionVo.getOpenid());
                WxRunService.me.saveOrUpdateWxRun(wxRun);
            }
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return wxRun;
    }

}
